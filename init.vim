" Le VimRC de Vitali64

" Check if the user uses at least Vim 8.0 or neoVim

if version < 800
	echo "Vim 8.0 or higher is required for the config file to work! Please upgrade! ~Vitali64"
	sleep 2
	quit
endif

" teh vimrc on the flies

autocmd	 BufWritePost .vimrc source $MYVIMRC


set incsearch

" Install Plugins with plug.vim
call plug#begin()
Plug 'tpope/vim-sensible'     " Sensible.vim
Plug 'joshdick/onedark.vim'     "OneDark Theme
Plug 'preservim/nerdtree'     "nerdTree
Plug 'scrooloose/syntastic'     "Syntastic (SYNtax, fanTASTIC, whatever)
Plug 'majutsushi/tagbar'     "TagBar
Plug 'vim-scripts/indentpython.vim'    "IndentPython
Plug 'neoclide/coc.nvim', {'branch': 'release'}     "ConquerOfCompletion 
Plug 'pangloss/vim-javascript'     "VIm JavaScript (Pour JS)
Plug 'sheerun/vim-polyglot' " Poligot.VIm
Plug 'tpope/vim-fugitive' " Support de GitHub/GitLab pour Vim
Plug 'mhinz/vim-startify' " Pour avoir un menu au demmarage de vim :) par Mhinz
call plug#end()

" Enable Syntax and filetype
filetype on
filetype plugin indent on
syntax on

" I want to see the number of lines
set number
set numberwidth=4

" file format and encoding
set fileformat=unix
set encoding=utf-8
set fileencoding=utf-8

" Startify-related things

let g:startify_custom_header = [
\ '					 _   __                 __ ',
\ '					| | / / __             / / ',
\ '					| |/ / /_/ _________  /_/  ',
\ '					| V / / / / /--//-// __    ',
\ '					|__/ /_/ /_/  // // /_/    ',
\ '		Vim is open source and freely distributable',
\ '',
\ "		Don't forget the basics !",
\ "		------",
\ "		:q   -> Quit buffer",
\ "		:q!  -> Rage quit buffer (without saving)",
\ "	    :qa  -> Quit all buffers",
\ "	    :qa! -> Rage quit all buffers (without saving)",
\ "	    :w   -> Save buffer",
\ "	    :wa  -> Save all buffers",
\ "	    :wq  -> Save and quit buffer",
\ "	    :wqa -> Save and quit all buffers",
\ "	    :wq! -> Save and rage quit buffer",
\ "	    (Press <ESC> before typing these commands)",
\]

" Working with tabs
set tabstop=4
set shiftwidth=4
set softtabstop=4
set noexpandtab

" Define characters to show up when there i a tab/eol or trailing spaces
" without ANY plugin
set listchars=tab:»\ ,eol:¬
set listchars+=trail:•
set list

" hwewwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww-------------

" indent/unindent with tab/shift-tab
nmap <Tab> >>
imap <S-Tab> <Esc><<i
nmap <S-tab> <<

" Theme
set background=dark
colorscheme onedark
hi Normal guibg=NONE ctermbg=NONE

" Restore the cursor place in a file
autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Git in the status bar
set statusline=%<%f\ %h%m%r%{FugitiveStatusline()}%=%-14.(%l,%c%V%)\ %P "HELLOOOO"

" Install CoC pLuGiNs
function InstallCocPlugs()
	echo "Installing ConquerOfCompletion Extensions ..."
	
	CocInstall coc-python coc-vimlsp coc-utils coc-tsserver coc-terminal coc-template coc-snippets coc-sh coc-prettier coc-pairs coc-lit-html coc-json coc-html coc-eslint coc-css


endfunction

" Install Plug.Vim

function InstallPlugDotVim()
    echo "Installation de vimplug ..."
    !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endfunction

" Copier, couper et coller
vmap <C-c> "+y
vmap <C-x> "+c
vmap <C-v> c<ESC>"+p
imap <C-v> <ESC>"+pa

" Automatically go into copy-paste mode when pasting in insert mode
" source: https://coderwall.com/p/if9mda/automatically-set-paste-mode-in-vim-when-pasting-in-insert-mode
let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"

function! XTermPasteBegin()
    set pastetoggle=<Esc>[201~
    set paste
    return ""
endfunction
inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()

" C++ specific things for me

" I sometimes forget to put std:: and that gives me compiling errors so...
" And I'm way too cool for namespaces
iab cout std::cout
iab endl std::endl
iab ostream std::ostream
iab vector std::vector
" Because I'm too lazy to type std::, I use vim abbreviations
